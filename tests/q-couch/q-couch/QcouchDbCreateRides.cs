using System;
using System.Collections.Generic;

namespace Qcouch
{
	public class QcouchDbCreateRides : QcouchDb
	{
		public void CreateSomeRides()
		{
			var list= new List<object>{
				new{name="fast one", description="it is fast"},
				new{name="slow one", description="it is slow"},
				new{name="another one", discription="cats like it"}
			};
			CreateSomeRecords(list, CreateRide);
		}

		public void CreateSomeRideStatus()
		{
			var list = new List<object> {
				new{
					ride_name="fast one",
					wait_time_min=11,
					state="closed"
				},
				new{
					ride_name="slow one",
					wait_time_min=17,
					state="open"
				},
				new{
					ride_name="another one",
					wait_time_min=120,
					state="open"
				}
			};
			CreateSomeRecords(list, CreateRideStatus);
		}

		public void CreateSomeWaitTimeModifiers()
		{
			var list = new List<object> {
				new{
					name="bronze",
					percentage=100,
					foreground_colour="#000000",
					background_colour="#CD7F32",
				},
				new{
					name="silver",
					percentage=50,
					foreground_colour="#000000",
					background_colour="#c0c0c0"
				},
				new {
					name="gold",
					percentage=5,
					foreground_colour="#000000",
					background_colour="#ffd700"
				},
				new {
					name="instant",
					percentage=0,
					foreground_colour="#ffffff",
					background_colour="#00ff00"
				}
			};
			CreateSomeRecords(list, CreateWaitTimeModifiers);

		}
	}
}

