function (newDoc, oldDoc, userCtx, secObj) {
    ////////////////////////////////////////////////////////////////
    //helpers
    
    var v = require("lib/validate").init(newDoc, oldDoc, userCtx, secObj);

    ////////////////////////////////////////////////////////////////
    //checks

    // admins can always delete
    if (v.isAdmin()) return true;
    
    //all docs must have these fields
    v.require("type");
    //v.require("created_at");
    //v.require("author");
    
    //don't change
    v.unchanged("type");  
    //v.unchanged("created_at");
    v.unchanged("author");

    //correct field format
    v.dateFormat("created_at");
    v.dateFormat("date");
    
    v.money("default_unit_price");
    v.money("unit_price");

    // :todo: iterate looking for IDs, etc    
    v.id2("default_pay_in_account", "account");
    v.id2("from_account", "account");
    v.id2("to_account", "account");
    
    v.id("unitType");
    v.id2("default_unitType", "unitType");
    
    v.id("transport")
    v.id2("defaultTransport", "transport");
    
    v.id("location");
    v.id("customer");
    
    v.number("default_number_of_units")
    v.number("units_sold");
    
    v.number("distance");
    
    v.postcode("my_postcode");
    v.postcode("their_postcode");

    //v.bool("is_default");
    
    //validate user
    //v.onlyAuthor();
    
    //check type
    v.validType("sale", "customer", "unitType",
                "transport", "location", "account",
                "payment", "expences");

    //
    switch (newDoc.type) {
    case "customer":
        v.require("name");
        v.require("default_unit_price");
        v.require("default_unitType_id");
        v.require("default_number_of_units");
        v.require("default_pay_in_account");
        break;
        
    case "unitType":
        v.require("name");
        break;

    case "transportType":
        v.require("name");
        break;

    case "location":
        v.require("name");
        v.require("customer_id");
        v.require("is_default");
        v.require("my_postcode");
        v.require("their_postcode");
        v.require("distance");
        break;

    case "sale":
        v.require("customer_id");
        v.require("date");
        v.require("unit_price");
        v.require("units_sold");
        v.require("unitType_id");
        v.require("pay_in_account_id");
        v.require("description");
        v.require("location_id");
        break;
    case "account":
        v.require("name");
        break;
    case "payment":
        v.require("customer_id");
        v.require("date");
        v.require("from_account_id");
        v.require("to_account_id");
        v.require("amount");
        break;
    case "expences":
        v.require("date");
        v.require("amount");
        v.require("description");
        break;
    }//end switch newDoc.type
}
