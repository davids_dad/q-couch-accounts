function downloadScript(script) {
    var element = document.createElement("script");
    element.src = script;
    document.body.appendChild(element);
}
downloadScript("/_utils/script/jquery.js");
downloadScript("/script/knockout-3.4.2.debug.js");

test( "ForeignKeys_values", function() {
    var testValue = [ "A test value" ];

    var iut = new ForeignKeys();
    iut._values["zzzz"]= testValue;
    iut._values["yyyy"]= "another value";

    ok( iut.values("zzzz") === testValue, "retrieves ok" );
});

test( "ForeignKeys_load", function() {
    var type="zzzz";
    var rows = [ 
	    { "value": { "name": "test1" } },
	    { "value": { "name": "test2" } },
	    { "value": { "name": "test3" } }
    ];

    var data = {
	    "rows": rows
    };

    var iut = new ForeignKeys();
    function jsonFetch_mock(url, callback) {
	    equal( url,  "/view/name-id-by-type/"+type );
	    callback(data);
    }
    iut._jsonFetch = jsonFetch_mock;
    iut._newObservableArray = function() {return ko.observableArray(); }

    var values = iut.values(type)();
    deepEqual( values, rows);
});

test( "guid", function() {
    var iut = new GuidMaker();
    var is_hexadecimal_re= new RegExp("^[0-9a-f]{32}$","i");
    var guids = new Array();
    const re = /([^_]*)_([0-9a-f]+)/;
    for (var i=0; i<100; i++) {
	    stop();
	    iut.Guid( "abcd", function(guid) {
            const found= [...guid.matchAll(re)];
            const _guid=found[0][2];
            const pre=found[0][1];
            equal( pre, "abcd");
	        equal(_guid.length,32);
	        ok( is_hexadecimal_re.test(_guid), "guid is hex");
	        ok( $.inArray(_guid, guids) == -1, "guid is unique" );
	        guids.push(_guid);
	        start();
	    });
    }
});
