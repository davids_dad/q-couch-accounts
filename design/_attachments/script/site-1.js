function koArrayReplaceAll(toKoArray, fromArray){
    var toArray = toKoArray();
    toArray.length=0;
    Array.prototype.push.apply(
        toArray,
        fromArray
    );
    toKoArray.valueHasMutated();
}

//----------------------------------------------------------------
//class KnockoutModelSynchronizedToCouchdb

function KnockoutModelSynchronizedToCouchdb(){
    var self=this;

    self.table = new Object();
    self.table["rows"] = ko.observableArray();   

    self.guidMaker = new GuidMaker();
    self.foreignKeys= new ForeignKeys(self);
    self.tableDescriptions= new TableDescriptions();

    self._jsonFetch = _jsonFetch_asyncAjax;

    _koSubscribe( self.ViewUrl(), function(newValue) {
        self.load(newValue);
    });

    ko.applyBindings(self);
};

KnockoutModelSynchronizedToCouchdb.prototype.ViewUrl=function(){
    var self=this;
    var result =  self.tableDescriptions.viewUrl();
    return result;
};

KnockoutModelSynchronizedToCouchdb.prototype.load = function(url){
    var self=this;
    self._jsonFetch(url, data => self._updateModelFromView(data));
};

KnockoutModelSynchronizedToCouchdb.prototype._updateModelFromView= function(data) {
    var self=this;
    var rows=data["rows"];

    self.table["rows"].removeAll(); //:todo: optimise
    
    $.each(rows, function(index, row) {
        self.addRowToTable(row["doc"]);
    });
    _updateScroll();
};

KnockoutModelSynchronizedToCouchdb.prototype.addRowToTable= function(row){
    var self=this;
    var value = new Object();
    var original = new Object();

    var cols=self.tableDescriptions.uiColumns()();
    $.each(cols, function(index, v) {
        var name = v.dbName;
        value[name] = ko.observable();
        original[name] = null;
    });

    $.each(row, function(index, v) {
        _koCreateObservable(value,    index, row[index]);
        if ( ko.isObservable (row[index] )){
            original[index]= row[index]();
        }else{
            original[index]= row[index];
        }
        //_koCreateObservable(original, index, row[index]);
    });
    
    var o = new Object();
    
    o["value"]=value;
    o["original"]=original;

    var isChangedTrigger=ko.observable().extend({ notify: 'always'});
    var isChanged = ko.computed({
        read: function() {
            var row = this;
            var dummy= isChangedTrigger();
            var result = false;
            $.each(row.value, function(index,v) {
                const this_changed = row.value[index]() != row.original[index];
                result = result || this_changed;
            });
            return result;
        },
        write: function(v){
            isChangedTrigger(null);
        }
    },o);
    o["isChanged"]=isChanged;
    
    var rev = ko.computed( function(){
        var row = this;
        var rev = row.value["_rev"]()
        return rev==null?"":rev.split("-")[0];
    }, o);
    o["rev"]=rev;
    
    self.table.rows.push(o);
    //notify
    isChanged(null);
};

KnockoutModelSynchronizedToCouchdb.prototype.newRow= function(){
    //:bookmark: this is where default value is done
    var self=this;
    const tableDescription=self.tableDescriptions._rawThisType();
    const type = tableDescription.type;
    self.guidMaker.Guid(type, function(guid) {
        var row = {
            _id:guid,
            _rev:undefined,
            type: type
        };
        
        $.each(tableDescription.columns, function(index, column) {
            var value
            if (column.defaultFtype){
                var data = ko.observable(null);
                value = ko.computed({
                    read: function(){
                        if (data() == null){
                            const ftype=column.defaultFtype;
                            const id=row[ftype+"_id"]();
                            const is_ffJoin = column.defaultJoinType;
                            const jtype = is_ffJoin?
                                  column.defaultJoinType:
                                  ftype;
                            const frecs = self.foreignKeys.values(jtype)();
                            const frec = is_ffJoin?
                                  frecs.filter(
                                      item => ( item.doc[ftype+"_id"] === id
                                                && item.doc.is_default )
                                  )[0]:
                                  frecs.filter(item => item.id === id )[0];
                            if (frec) {
                                const info = frec.doc;
                                if (column.defaultFfield){
                                    const result = info[column.defaultFfield];
                                    return result;
                                }else{
                                    const result = info.name;
                                    return result;
                                }
                            }else{
                                return null;
                            }
                        }else{
                            return data();
                        }
                    },
                    write: function (value) {
                        data(value);
                    }
                });
            }else if (column.defaultValue){
                value = ko.observable(column.defaultValue);
            }else{
                value = ko.observable("");
            }
               
            row[column.dbName]=value;
        });
        
        self.addRowToTable(row);
    });
    _updateScroll();
};


KnockoutModelSynchronizedToCouchdb.prototype.revertFromUndo= function(){
    var self=this;
    $.each(self.table.rows(), function(index, row) {
        self.revertRowFromUndo(row);
    });
};

KnockoutModelSynchronizedToCouchdb.prototype.commitRowToUndo= function(row) {
    var self=this;
    $.each(row["value"], function(index, v) {
        var newValue= v();
        row["original"][index]=newValue;
    });
};

KnockoutModelSynchronizedToCouchdb.prototype.revertRowFromUndo= function(row){
    var self=this;
    $.each(row["original"], function(index, newValue) {
        row["value"][index](newValue);
    });
};

KnockoutModelSynchronizedToCouchdb.prototype.save= function(){
    var self=this;
    function saveRow(index, row){
        if (row.isChanged) {

            var value= row.value;

            //When working move paragraph to own method
            const type = self.tableDescriptions.viewId();
            const typeInfo = self.tableDescriptions._data.types[type].columns;
            typeInfo.filter(
                item => item.type === "bool"
            ).map (
                item => item.dbName
            ).forEach(function(item){
                if ( value[item] === true   ||
                     value[item] === "true" ){
                    value[item] = true;
                }else{
                    value[item] = false;
                }
            });
            
            $.ajax({
                url: "db/"+value._id, 
                type: "put",
                data: JSON.stringify(value),
                success:  function(responce_json) {
                    var responce=JSON.parse(responce_json);
                    var rev=responce["rev"];
                    var row=self.table.rows()[index];
                    var value = row.value;
                    value["_rev"](rev);
                    self.commitRowToUndo(row);
                }
            });
        }
    }

    $.each(self.table.rows(), function (index, row) {
        const js_row= ko.toJS(row);
        saveRow(index,js_row);
    });
};

KnockoutModelSynchronizedToCouchdb.prototype.row = function(f_row, rows) {
    var self=this;
    return ko.computed( function(){
        $.each( rows(), function(index, row) {
            if(self.cmp( f_row, row )) {
                return [row];
            }
        } );
        return [];
    });
}

//:todo: is this used
KnockoutModelSynchronizedToCouchdb.prototype.cmp = function( f_row, row ){
    var self=this;
    var f_id = f_row.id;
    var s_id = row.value.ride();
    var result = f_id == s_id;
    return result;
}

KnockoutModelSynchronizedToCouchdb.prototype.changeViewId= function(viewId){
    var self=this;
    self.tableDescriptions.viewId(viewId);
}

//----------------------------------------------------------------
//class TableDescriptions

function TableDescriptions() {
    var self=this;
    self.viewId=ko.observable(null);
    self._newObservableArray = _newKoArray;
    self._newObservable = _newKo;
    self._jsonFetch = _jsonFetch_asyncAjax;

    self._managerMenu = self._newObservableArray();
    self._uiColumns = self._newObservableArray();
    self._meta = self._newObservable();
    
    self._load();
};

TableDescriptions.prototype._load= function(){
    var self=this;

    self._jsonFetch(
        "/table-descriptions",
        function( data ) {
            self._data=data;
            
            self.calculateManagerMenu();
            self.viewId.subscribe( function(newValue) {
                self.calculateMeta();
                self.calculateUiColumns(); //:todo: could be done once, but defered.
                //self.calculateMeta();
            });
        }
    );
};

TableDescriptions.prototype._commonColumns=function(){
    var self=this;
    return self._data.common.columns;
};

TableDescriptions.prototype.calculateUiColumns=function(){
    var self=this;
    self._uiColumns.removeAll();
    $.each(self._commonColumns(), function(index, col) {
        self._uiColumns.push(col);
    });
    $.each(self._rawThisType().columns, function(index, col) {
        self._uiColumns.push(col);
    });
};

TableDescriptions.prototype.uiColumns=function(){
    var self=this;
    return self._uiColumns;
};

TableDescriptions.prototype.calculateManagerMenu=function(){
    var self=this;
    $.each(self._data.managerMenu, function(index, row){
        self._managerMenu.push(row);
    });
};

TableDescriptions.prototype.managerMenu=function(){
    var self=this;
    return self._managerMenu;
};

TableDescriptions.prototype._rawThisType=function() {
    var self=this;
    return self._data.types[self.viewId()];
};

TableDescriptions.prototype.calculateMeta=function() {
    var self=this;
    self._meta( self._rawThisType().meta );
};

TableDescriptions.prototype.meta=function(name) {
    var self=this;
    return ko.computed( function() {
        var meta = self._meta();
        return meta ? meta[name] : null;
    });
};

TableDescriptions.prototype.viewUrl=function() {
    var self=this;
    return self.meta("view");
};

TableDescriptions.prototype.each=function() {
    var self=this;
    return self.meta("each");
};

//----------------------------------------------------------------
//standalone private methods

_jsonFetch_asyncAjax= function(url, callback){
    $.ajax({
        dataType: "json",
        url: url,
        success: callback
    });
}

_newKoArray = function() {
    return ko.observableArray();
}

_newKo = function() {
    return ko.observable();
}

_koSubscribe = function(observable, callback){
    observable.subscribe( callback );
    
    if (observable()) {
        observable.valueHasMutated();
    }
}

_koCreateObservable = function(parentObject, newName, newValue){
    if ( ko.isObservable (newValue) ){
        parentObject[newName]=newValue;  
    //}
    //else if (parentObject[newName]){
    //    parentObject[newName](newValue);
    }else{
        parentObject[newName] = ko.observable(newValue);
    }
}

_findInArray = function( array, key ) {
    $.each(array, function(index, item) {
        //if (item.value.Guid
    });
    return null;
}

_updateScroll = function () {
    var element = document.getElementById("bottom_scroll");
    element.scrollTop = element.scrollHeight;
}

//----------------------------------------------------------------
//class ForeignKeys

function ForeignKeys(root){
    var self=this;
    self.root=root;
    self._jsonFetch = _jsonFetch_asyncAjax;
    self._newObservableArray = _newKoArray;
    self._values= new Array();
}

ForeignKeys.prototype._load= function(type){
    var self=this;
    if ( !self._values[type] ){ //once
        self._values[type] = self._newObservableArray();
        self._jsonFetch(
            "/view/name-id-by-type/"+type,
            function( data ) {
                var arr = new Array();
                $.each(data.rows, function(index, row) {
                    arr.push(row);
                });
                koArrayReplaceAll(
                    self._values[type],
                    arr
                );
            }
        );
    }
};

ForeignKeys.prototype.values= function(type){
    var self=this;
    self._load(type);
    var result = self._values[type];
    return result;
};

ForeignKeys.prototype.fkeyByName= function(type){
    var self=this;
    return self.values(type);
}

ForeignKeys.prototype.fkeyByNameFiltered= function(type, peerName, row){
    var self=this;
    var arr = new ko.observableArray([]);

    if (peerName in row.value) {
        const peer=row.value[peerName];
        const currentPeerId=peer();
        
        const fullKoArray = self.values(type);
        const fullArray = fullKoArray();
        const newArray = fullArray.filter(
            item => item.doc[peerName] == currentPeerId
        );
        
        koArrayReplaceAll(
            arr,
            newArray
        );
    }else{
        //do nothing.
        //:tricky: this conditional, is to deal with a glitch where model is updating.
    }
    
    return arr;
};

ForeignKeys.prototype.fkeyByNameSelfFiltered= function(type, predicate){
    var self=this;
    var arr = new ko.observableArray([]);
        
    const fullKoArray = self.values(type);
    const fullArray = fullKoArray();
    const funcBody = "return " +predicate+ ";";
    const f = new Function("item", funcBody);
    const newArray = fullArray.filter(
        item => f(item.doc)
    );
    
    koArrayReplaceAll(
        arr,
        newArray
    );
    
    return arr;
};


//----------------------------------------------------------------
//class GuidMaker

function GuidMaker(){
    var self=this;
    self.emptyThreshold=5;
    self.fetchQuantity=16;
    
    self.url="/_uuids?count="+self.fetchQuantity;

    self.requests1= -self.fetchQuantity;
    self.requests2= -self.fetchQuantity;
    self.guids=[];
    self.ajax=new Array();
    self._getSomeGuids();
};

GuidMaker.prototype._getSomeGuids= function(){
    var self=this;
    
    const r=$.ajax({
        dataType: "json",
        url: self.url,
        cache: false,
        success: function( data ) {
            self.guids=$.merge(self.guids, data.uuids);
        }
    });
    self.ajax.push(r);
};

GuidMaker.prototype._nextGuid= function(){
    var self=this;
    var result= self.guids.pop();
    return result;
};

GuidMaker.prototype.Guid= function(type, callback){
    var self=this;

    self.requests1 += 1;
    self.requests2 += 1;

    if ( self.requests1 > -self.emptyThreshold )
    {
        self.requests1 -= self.fetchQuantity;
        self._getSomeGuids();
    }

    if ( self.requests2 > 0 )
    {
        self.requests2 -= self.fetchQuantity;
        self.ajax.shift();
    }

    $.when(self.ajax[0]).then( 
        function() {
            callback(type+ "_" +self._nextGuid());
        }
    );
};
