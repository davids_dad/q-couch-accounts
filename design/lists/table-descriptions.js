function  (head, req) {
    //serve json from design, not from main docs. Ignores view.
 
    var tableDescriptions = require("lib/tableDescriptions").all();

    function stash(){
	    return tableDescriptions;
    }

    provides("json", function() {
	    return JSON.stringify(stash());
    });   
}
