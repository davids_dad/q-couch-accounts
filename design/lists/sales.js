function (head, req) {
    var ddoc = this;
    const Mustache = require("lib/mustache");
    const List = require("vendor/couchapp/lib/list");
    const path = require("vendor/couchapp/lib/path").init(req);
    const myLib = require("lib/myLib");
    const template = req.query.template;

    //-----------------------------------------------------------------------------------------

    const startDate = req.query["startDate"];
    const endDate = req.query["endDate"];
    const title = req.query["title"]+ " " +startDate+ " to " +endDate;

    function Capitalise (word) {
        return word.charAt(0).toUpperCase() + word.slice(1);
    }
    
    function Stash()
    {
        var body = [];
        var total = 0;
        function process(row){
            if (row!=null){
                const value = row.value.value;
                const name = row.doc[req.query.firstCol];
                const date = row.key[row.key.length-1];
                total+=value*100;
                const record = [
                    name,
                    date,
                    value.toString(),
                    (total/100).toString()
                ];
                body.push(record);
            }else{
                
            }
        }

        function mainLoop(){
            while (row = getRow() ) {
                process(row);
            }
            process(null);
        }

        mainLoop();

        return {
            title: title,
            head: [ ( req.query.firstCol?
                      Capitalise(req.query.firstCol) :
                      null
                    ),
                    "Date", "Amount", "Total"],
            body: body,
            foot: [["Sum", "", "", (total/100).toString()]]
        };
    }

    //-- The provides function serves the format the client requests.
    //-- The first matching format is sent, so reordering functions changes 
    //-- thier priority. In this case HTML is the preferred format, so it comes first.

    if ( req.query.csv === undefined ){
        provides("html", function() {
            return Mustache.to_html(ddoc.templates[template],  myLib.addIfdef(Stash()), ddoc.templates.partials);
        });

        provides("json", function() {
            //return JSON.stringify(req);
            return JSON.stringify(Stash());
        });
   }

    provides("csv", function() {
        const stash= Stash();
        var store=new myLib.CsvOutputStore();
        
        store.WriteLine ([stash.title]);
        store.WriteLine (stash.head);
        store.WriteLines (stash.body);
        store.WriteLine (stash.foot);
        return store.Result();
    });
};
