"use strict";
function (head, req) {
    const ddoc = this;
    const Mustache = require("lib/mustache");
    const List = require("vendor/couchapp/lib/list");
    const path = require("vendor/couchapp/lib/path").init(req);
    const myLib = require("lib/myLib");

//-----------------------------------------------------------------------------------------
    const template = req.query["template"];

    const query_types=[
        { id: "sales-by-date", name: "Sales" },
        { id: "travel-expences-by-date", name: "Travel" },
        { id: "expences-by-date", name: "Expences" }
    ];

    const top_query_types=[
        { id: "index", name: "Main menu" },
        { id: "outstanding-by-customer", name: "Outstanding by customer" }
    ];

    const dates=[
        null,
        { type:"All Time", name: "All Time" },
        { type:"0", start: "_",          end: "2018-04-05", name: "initial period to 2018-04-05" },
        { type:"0", start: "2018-04-06", end: "2018-05-29", name: "new tax year to company start 2018-04-06 to 2018-05-29" },
        { type:"0", start: "2018-05-30", end: "2019-05-29", name: "first year 2018-05-30 to 2019-05-29" },
        { type:"0", start: "2019-05-30", end: "2020-05-29", name: "second year 2019-05-30 to 2020-05-29" },
        { type:"0", start: "2020-05-30", end: "2021-05-29", name: "third year 2020-05-30 to 2021-05-29" },
        { type:"0", start: "2021-05-30", end: "2022-05-29", name: "forth year 2021-05-30 to 2022-05-29" },
        { type:"0", start: "2022-05-30", end: "2023-05-29", name: "fith year 2022-05-30 to 2023-05-29" },
        { type:"0", start: "2023-05-30", end: "2024-05-29", name: "sixth year 2023-05-30 to 2024-05-29" },
        { type:"0", start: "2024-05-30", end: "2025-05-29", name: "seventh year 2024-05-30 to 2025-05-29" },
        { type:"0", start: "2025-05-30", end: "2026-05-29", name: "eight year 2025-05-30 to 2026-05-29" }
    ];

    function section(date){ 
        var items = [];
        if (date === null) {
            top_query_types.forEach( function(item) {
                items.push({
                    url: "/" +item.id,
                    name: item.name
                });
            });
        } else {
            const datePart=     date.type=="All Time"? "": date.start+ "/to/" +date.end;
            const fileDatePart= date.type=="All Time"? "": "/" +datePart;
            const urlDatePart=  date.type=="All Time"? "": "/" +datePart;
            query_types.forEach( function(item) {
                items.push({
                    url: "/" +item.id+urlDatePart,
                    name: item.name,
                    csv: {
                        fileName: item.id+fileDatePart+ ".csv",
                        url: "/" +item.id+urlDatePart+ "?csv"
                    }
                });
            });
        }
        return items;
    }

    function stash(){
        var sections=[];
        dates.forEach (function(date) {
            sections.push({
                hideshow: date? "hideshow": "",
                heading: date? date.name: null,
                items: section(date)
            });
        });

        return {
            menu: sections
        };
    }



    //-- The provides function serves the format the client requests.
    //-- The first matching format is sent, so reordering functions changes 
    //-- thier priority. In this case HTML is the preferred format, so it comes first.

    provides("html", function() {
        return Mustache.to_html(ddoc.templates[template],  myLib.addIfdef(stash()), ddoc.templates.partials);
    });

    provides("json", function() {
        return JSON.stringify(stash());
    });   
};
