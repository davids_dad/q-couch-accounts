function (head, req) {
    var ddoc = this;
    const Mustache = require("lib/mustache");
    const List = require("vendor/couchapp/lib/list");
    const path = require("vendor/couchapp/lib/path").init(req);
    const myLib = require("lib/myLib");
    const template = req.query.template;

    //-----------------------------------------------------------------------------------------

    const startDate = req.query["startDate"];
    const endDate = req.query["endDate"];
    const title = req.query["title"]+ " " +startDate+ " to " +endDate;
    
    function Stash()
    {
        var customerName;
        var transportName;
        var transportId;
        var date;
        var location;
        var distance;
        var duration;
        var unitsSold;
        var unitType;
        var transports = {};
        var body = [];
        var total = 0;
        var _distance_totals = {};
        var _duration_totals = {};
        var totals = [];

        const totalName="_Total";
        transports[totalName]= "Total";

        function type(row){
            return row.key[3];
        }

        function sequence(row){
            return row.key[2];
        }

        function _addToTotal(total_object, id, value){
            const _value = value*1;
            if (total_object.hasOwnProperty(id)) {
                total_object[id] += _value;
            }else{
                total_object[id] = _value;
            } 
        }

        function addToTotal(){
            _addToTotal(_distance_totals, totalName, distance);
            _addToTotal(_distance_totals, transportId, distance);
            _addToTotal(_duration_totals, totalName, duration);
            _addToTotal(_duration_totals, transportId, duration);
        }

        function foreach( dict, func ){
            Object.keys(dict).forEach( func  );
        }
        
        function process(row){
            if (type(row)==="customer" ){
                customerName = row.doc.name;

            } else if (type(row)==="transport" ){
                transportName = row.doc.name;
                transportId = row.value._id;
                transports[transportId] = transportName;
                
            } else if (type(row)==="location" ){        
                date = row.key[0];
                location = row.doc.their_postcode;
                distance = row.doc.distance;
            } else if (type(row)==="sale" ){
                unitsSold = row.doc.units_sold;
            } else if (type(row)==="unitType" ){
                unitType = row.doc.name;
            }

            if (sequence(row)==="9" ){
                duration = (unitType=="hourly")?
                      unitsSold:
                      0;
                const record = [
                    customerName,
                    date,
                    location,
                    distance,
                    transportName,
                    duration
                ];
                body.push(record);
                total+=distance*1;

                addToTotal();
                
            }else{
                
            }
        }

        function mainLoop(){
            while (row = getRow() ) {
                process(row);
            }
        }

        mainLoop();

        const heading = ["Customer", "Date", "Location", "Distance (miles)", "Transport", "Duration (hours)"];
        totals.push(heading);
        
        foreach (_distance_totals, function (key) {
            totals.push([
                "", "", "",
                String(_distance_totals[key]),
                transports[key],
                String(_duration_totals[key]),
            ]);
        });

        return {
            title: title,
            head: heading,
            body: body,
            foot: totals
        };
    }

    //-- The provides function serves the format the client requests.
    //-- The first matching format is sent, so reordering functions changes 
    //-- thier priority. In this case HTML is the preferred format, so it comes first.

    if ( req.query.csv === undefined ) {
        provides("html", function() {
            return Mustache.to_html(ddoc.templates[template],  myLib.addIfdef(Stash()), ddoc.templates.partials);
        });
        
        provides("json", function() {
            //return JSON.stringify(req);
            return JSON.stringify(Stash());
        });
    }

    provides("csv", function() {
        const stash= Stash();

        var store=new myLib.CsvOutputStore();
        store.WriteLine ([stash.title]);
        store.WriteLine (stash.head);
        store.WriteLines(stash.body);
        store.WriteLines(stash.foot);
        
        return store.Result();
    });
};
