"use strict";
function (head, req) {
    const ddoc = this;
    const Mustache = require("lib/mustache");
    const List = require("vendor/couchapp/lib/list");
    const path = require("vendor/couchapp/lib/path").init(req);
    const myLib = require("lib/myLib");

//-----------------------------------------------------------------------------------------

    function Title() {
        const part1=req.query["title_part1"];
        const part2=req.query["title_part2"];
        const title=req.query["title"];
        
        return (
            title ? title:
                (part1 && part2) ? (part1+ " " +part2):
                part1 ? part1:
                 " "
        );
    }
    
    const template = req.query["template"];
    const main_url= req.query["main_url"];
    const subSite= req.query["subSite"];
    const edit= req.query["edit?"];

    function stash(){
        return {
	        title:Title(),
	        main_url:main_url,
	        subSite: subSite,
            "edit?": edit
	    };
    }



    //-- The provides function serves the format the client requests.
    //-- The first matching format is sent, so reordering functions changes 
    //-- thier priority. In this case HTML is the preferred format, so it comes first.

    provides("html", function() {
        return Mustache.to_html(ddoc.templates[template],  myLib.addIfdef(stash()), ddoc.templates.partials);
    });

    provides("json", function() {
        return JSON.stringify(stash());
    });   
};
