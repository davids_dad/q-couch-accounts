"use strict";
function (head, req) {
    const ddoc = this;
    const Mustache = require("lib/mustache");
    const List = require("vendor/couchapp/lib/list");
    const path = require("vendor/couchapp/lib/path").init(req);
    const myLib = require("lib/myLib");

//-----------------------------------------------------------------------------------------

    function title() {
        const part1=req.query["title_part1"];
        const part2=req.query["title_part2"];
        const title=req.query["title"];
        
        return (
            title ? title:
                (part1 && part2) ? (part1+ " " +part2):
                part1 ? part1:
                 " "
        );
    }

    function type(row) {
        return row.key[3];
    }

    function sequence(row) {
        return row.key[2];
    }

    function accountId(row) {
        return row.key[1];
    }

    function uniq_push(items, customer_id, customer_name, account_id, account_name) {
        const prev_item = (items.length > 0) ? items[ items.length - 1 ] : null;
        const current_id=customer_id+account_id;
        var item;
        if ( prev_item === null || prev_item.id !== current_id) {
            if (  prev_item === null || prev_item.customer_id !== customer_id ) {
                item = {
                    id: current_id,
                    customer_id: customer_id,
                    name: customer_name,
                    url: "outstanding-by-customer/" +customer_id
                };
                items.push(item);
            }
            item = {
                id: current_id,
                customer_id: customer_id,
                name: customer_name+ "—" + (account_name?account_name:account_id) ,
                url: "outstanding-by-customer-account/" +customer_id+ "/" +account_id
            };
            items.push(item);
        }
    }

    function stash(){
        var items = [],
            customer_name,
            customer_id,
            account_name,
            account_id;
        
        function process(row){
            var item;
            var is_partial=true;
            if ( type(row) === "customer" ) {
                customer_name=row.doc.name;
                customer_id=row.doc._id;
                account_id=accountId(row);
                account_name=null;
            }
            if ( type(row) === "account" ) {
                is_partial = (row.doc.is_primary != row.doc.is_secondary);
                account_name=row.doc.name;
            }
            if ( sequence(row) === "9" && is_partial ) {
                uniq_push(items, customer_id, customer_name, account_id, account_name); 
            }
        }

        function mainLoop(){
            var row;
            while (row = getRow() ) {
                process(row);
            }
        }
        
        mainLoop();
        
        return {
	        title: title(),
            items: items
	    };
    }


    //-- The provides function serves the format the client requests.
    //-- The first matching format is sent, so reordering functions changes 
    //-- thier priority. In this case HTML is the preferred format, so it comes first.

    provides("html", function() {
        return Mustache.to_html(ddoc.templates[req.query["template"]],  myLib.addIfdef(stash()), ddoc.templates.partials);
    });

    provides("json", function() {
        return JSON.stringify(stash());
    });   
};
