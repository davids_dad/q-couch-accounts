"use strict";
function (head, req) {
    var ddoc = this;
    const Mustache = require("lib/mustache");
    const List = require("vendor/couchapp/lib/list");
    const path = require("vendor/couchapp/lib/path").init(req);
    const myLib = require("lib/myLib");
    const template = req.query.template;

    //-----------------------------------------------------------------------------------------

    const startDate = req.query["startDate"];
    const endDate = req.query["endDate"];
    const title = req.query["title"];

    function stash()
    {
        var body = [];
        var total = 0;
        var customer_name;
        var accountNameA;
        var accountNameB;
        var is_partial;

        function Date(row){
            return row.key[2];
        }
        function Type(row){
            return row.key[4];
        }
        function Name(row){
            return row.doc.name;
        }
        function Value(row){
            return row.value.value;
        }
        function IsPartial(row){
            return row.doc.is_primary && !row.doc.is_secondary;
        }
        function do_transact(row){
            const value = Value(row).toString();
            const type = Type(row);
            if (is_partial){
                total+=Value(row)*100;
                const record = [
                    type,
                    customer_name,
                    Date(row),
                    accountNameA,
                    accountNameB,
                    value,
                    total/100
                ];
                body.push(record);
            }
        }

        function process(row){

            if (row!=null){

                if (Type(row) === "customer"){
                    customer_name = Name(row);
                }
                else if (Type(row) === "accountA"){
                    accountNameA = Name(row);
                    accountNameB = "nill";
                    is_partial = IsPartial(row);
                }
                else if (Type(row) === "accountB"){
                    accountNameB = Name(row);
                }
                else if (Type(row) === "sale" || Type(row) === "payment"){
                    do_transact(row);
                }
            }
        }

        function mainLoop(){
            var row;
            while (row = getRow() ) {
                process(row);
            }
            process(null);
        }

        mainLoop();

        return {
            title: title,
            head: ["type", "Name", "Date", "account A", "account B", "Amount", "Total"],
            body: body,
            foot: [["Outstanding", "", "", "", "", "", (total/100).toString()]],
            aside: {url: "/menu-of-outstanding-by-customer"} //standin for now.
        };
    }

    //-- The provides function serves the format the client requests.
    //-- The first matching format is sent, so reordering functions changes
    //-- thier priority. In this case HTML is the preferred format, so it comes first.

    provides("html", function() {
        return Mustache.to_html(ddoc.templates[template],  myLib.addIfdef(stash()), ddoc.templates.partials);
    });

    provides("json", function() {
        return JSON.stringify(stash());
        //return JSON.stringify(myLib.addIfdef(stash()));
    });
};
