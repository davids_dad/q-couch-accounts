function(doc) {
    // !code  lib/fragments/isDesignDoc.js

    function key ( account, processOrder, type){
        const date=doc.date;
        return [doc.customer_id, account, date, processOrder, type];
    }

    if (!is_Design() &&
        doc.type === "sale" &&
        !doc.removed ) {
        const value = doc.units_sold * doc.unit_price;
        const pay_in_account = doc.pay_in_account_id;
        emit(key(pay_in_account, "00", "customer"), {
            _id: doc.customer_id
        });
        emit(key(pay_in_account, "00", "accountA"), {
            _id: doc.pay_in_account_id
        });
        emit(key(pay_in_account, "09", "sale"), {
            value: value
        });
    }
    if (!is_Design() &&
        doc.type === "payment" &&
        !doc.removed ) {
        const account_from = doc.from_account_id;
        emit(key(account_from,"10","customer"), {
            _id: doc.customer_id
        });
        emit(key(account_from,"10","accountA"), {
            _id: doc.from_account_id
        });
        emit(key(account_from,"11","accountB"), {
            _id: doc.to_account_id
        });
        emit(key(account_from,"19","payment"), {
            value: -doc.amount
        });

    }
};
