function(doc) {  
    // !code  lib/fragments/isDesignDoc.js
    
    if (!is_Design() &&
        !doc.removed ){

        if (doc.type === "sale") {
            emit([ doc.customer_id, doc.pay_in_account_id, "0", "customer"], {
                _id: doc.customer_id
            });
            emit([ doc.customer_id, doc.pay_in_account_id, "9", "account"], {
                _id: doc.pay_in_account_id
            });
        }
        if  (doc.type === "payment") {
            emit([doc.customer_id, doc.from_account_id, "9", "customer"], {
                _id: doc.customer_id
            });
        }
	}
};
