function(doc) {
    // !code  lib/fragments/isDesignDoc.js
   
    if (!is_Design()){
	    if ( "name" in doc ) {
	        emit(
                [doc.type, doc.name],
                {
		            name: doc.name,
		            removed: doc.removed
	            }
            );
	    }  
    }
};
