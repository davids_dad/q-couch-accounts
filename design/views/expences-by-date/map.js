function(doc) {  
    // !code  lib/fragments/isDesignDoc.js
   
    if (!is_Design() &&
        doc.type === "expences" &&
        !doc.removed ) {
        emit([doc.date],{
            value: doc.amount
        });
    }
};
