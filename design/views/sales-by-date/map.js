function(doc) {  
    // !code  lib/fragments/isDesignDoc.js
   
    if (!is_Design() &&
        doc.type === "sale" &&
        !doc.removed ) {
        emit([doc.date],{
            value: doc.units_sold * doc.unit_price,
            _id: doc.customer_id
        });
    }
};
