//index by name where possible, with some exceptions.
//we may need to add something to block some types for performance/memory.

function(doc) {  
    // !code  lib/fragments/isDesignDoc.js
   
    if (!is_Design()){
        const name = doc.name || null;
        const date = doc.date || null
        const k= name || date; 

        emit([doc.type,k], null);
    }
};
