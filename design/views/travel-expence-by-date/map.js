function(doc) {  
    // !code  lib/fragments/isDesignDoc.js
   
    if (!is_Design() &&
        doc.type === "sale" &&
        !doc.removed ) {
        emit([doc.date, doc._id, "0", "customer"],{
            _id: doc.customer_id
        });
        emit([doc.date, doc._id, "0", "transport"],{
            _id: doc.transport_id
        });
        emit([doc.date, doc._id, "0", "unitType"],{
            _id: doc.unitType_id
        });
        emit([doc.date, doc._id, "0", "location"],{
            _id: doc.location_id
        });

        emit([doc.date, doc._id, "9", "sale"],{});
	}
};
