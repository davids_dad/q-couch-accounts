function today() {
    const now = new Date().toISOString();
    const i = now.indexOf("T")
    const date = now.substring(0,i)
    return date;
}

var tables = {
    common: {
        //rowHidden: "function(row) { return row.value.removed(); }",
        columns:  [
            {
                uiName : "Guid",
                type : "guid",
                dbName : "_id",
                hidden: true
            },
            {
                uiName : "Revision",
                type : "rev",
                dbName : "_rev",
                hidden: true
            },
            {
                uiName : "Removed",
                type : "removed",
                dbName : "removed",
                hidden : true
            }
        ]
    },
    managerMenu: [
        {
            uiName: "Sales",
            viewId: "sales"
        },{
            uiName: "Payments",
            viewId: "payments"
        },{
            uiName: "Expences",
            viewId: "expences"  
        },{
            viewId: "----"
        },{
            uiName: "Customers",
            viewId: "customers"
        },{
            uiName: "Locations",
            viewId: "locations"
        },{
            viewId: "----"
        },{
            uiName: "Unit types",
            viewId: "unitTypes"
        },{
            uiName: "Transport types",
            viewId: "transport"
        },{
            uiName: "Account",
            viewId: "accounts"  
        },{
            uiName: "removed records",
            viewId: "removed"
        }
    ],
    types : {
        removed: {
            meta: {
                view: "view/removed",
                rowHidden: false
            },
            columns: [
                {
                    uiName: "Type",
                    type: "text",
                    dbName: "type",
                    readOnly: true
                },
                {
                    uiName: "Name",
                    type: "text",
                    dbName: "name",
                    readOnly: true
                },
                {
                    uiName : "Removed",
                    type : "removed",
                    dbName : "removed",
                    readOnly: true
                },
                {
                    uiName : "id",
                    type : "link",
                    dbName : "_id",
                    readOnly: true
                }
            ]
        },
        customers : {
            type : "customer",
            meta: {
                view:  "view/by_name/customer"
            },
            columns: [
                {
                    uiName: "Name",
                    type: "text",
                    dbName: "name",
                    defaultValue: ""
                },
                {
                    uiName: "Default unit price",
                    type: "money",
                    dbName: "default_unit_price",
                    defaultValue: "25.00"
                },
                {
                    uiName: "Default unit type",
                    type:   "fkeyByName", ftype: "unitType",
                    dbName: "default_unitType_id",
                    defaultValue: "hourly"
                },
                {
                    uiName: "Default number of units",
                    type: "quantity",
                    dbName: "default_number_of_units",
                    defaultValue: "1"
                },
                {
                    uiName: "Default pay in account",
                    type:   "fkeyByNameSelfFiltered", ftype: "account",
                    predicate: "item.is_primary",
                    dbName: "default_pay_in_account",
                    defaultValue: ""
                }
            ]
        },
        expences : {
            type : "expences",
            meta: {
                view:  "view/by_name/expences"
            },
            columns: [
                {
                    uiName: "Date",
                    type:   "date",
                    dbName: "date",
                    defaultValue: today()
                },{
                    uiName: "Description",
                    type:   "text",
                    dbName: "description",
                    defaultValue: ""
                },{
                    uiName: "Amount",
                    type:   "money",
                    dbName: "amount"
                }
            ]
        },
        unitTypes :  {
            type : "unitType",
            meta: {
                view:  "view/by_name/unitType"
            },
            columns: [
                {
                    uiName: "Name",
                    type: "text",
                    dbName: "name",
                    defaultValue: ""
                }
            ]
        },
        transport :  {
            type : "transport",
            meta: {
                view:  "view/by_name/transport"
            },
            columns: [
                {
                    uiName: "Name",
                    type: "text",
                    dbName: "name",
                    defaultValue: ""
                }
            ]
        },
        locations : {
            type : "location",
            meta: {
                view:  "view/by_name/location"
            },
            columns: [
                {
                    uiName: "Name",
                    type: "text",
                    dbName: "name",
                    defaultValue: "Their house"
                },{
                    uiName: "Customer",
                    type:   "fkeyByName", ftype: "customer",
                    dbName: "customer_id",
                    defaultValue: ""
                },{
                    uiName: "Default?",
                    type:   "bool",

                    dbName: "is_default",
                    defaultValue: true
                },{
                    uiName: "My Postcode",
                    type:   "text",
                    dbName: "my_postcode",
                    defaultValue: "GU47 0DU"
                },{
                    uiName: "Their Postcode",
                    type:   "text",
                    dbName: "their_postcode",
                    defaultValue: ""
                },{
                    uiName: "Distance (miles)",
                    type:   "text",
                    dbName: "distance",
                    defaultValue: "0"
                },{
                    uiName: "Default Transport",
                    type:   "fkeyByName", ftype: "transport",
                    dbName: "defaultTransport_id",
                    defaultValue: ""
                }
            ]
        },
        accounts:{
            type : "account",
            meta: {
                view:  "view/by_name/account"
            },
            columns: [
                {
                    uiName: "Name",
                    type: "text",
                    dbName: "name",
                    defaultValue: "default"
                },{
                    uiName: "Primary?",
                    type:   "bool",
                    dbName: "is_primary",
                    defaultValue: ""
                },{
                    uiName: "Secondary?",
                    type:   "bool",
                    dbName: "is_secondary",
                    defaultValue: ""
                }
            ]
        },
        sales : {
            type : "sale",
            meta: {
                view:  "view/by_name/sale"
            },
            columns: [
                {
                    uiName: "Customer",
                    type:   "fkeyByName", ftype: "customer",
                    dbName: "customer_id",
                    defaultValue: ""
                },{
                    uiName: "Date",
                    type:   "date",
                    dbName: "date",
                    defaultValue: today()
                },{
                    uiName: "Unit Price",
                    type:   "money",
                    dbName: "unit_price",
                    defaultFtype: "customer", defaultFfield:"default_unit_price"
                },{
                    uiName: "Units sold",
                    type:   "quantity",
                    dbName: "units_sold",
                    defaultFtype: "customer", defaultFfield:"default_number_of_units"
                },{
                    uiName: "Units type",
                    type:   "fkeyByName", ftype: "unitType",
                    dbName: "unitType_id",
                    defaultFtype: "customer", defaultFfield:"default_unitType_id"
                },{
                    uiName: "Pay in account",
                    type:   "fkeyByNameSelfFiltered", ftype: "account",
                    predicate: "item.is_primary",
                    dbName: "pay_in_account_id",
                    defaultFtype: "customer", defaultFfield:"default_pay_in_account"
                },{
                    uiName: "Location", 
                    type:   "fkeyByNameFiltered", ftype: "location",
                    filterPeer: "customer_id", 
                    dbName: "location_id",
                    defaultFtype: "customer", defaultJoinType: "location",  defaultFfield: "_id"
                },{
                    uiName: "Transport", 
                    type:   "fkeyByName", ftype: "transport",
                    dbName: "transport_id",
                    defaultFtype: "location", defaultFfield: "defaultTransport_id"
                },{
                    uiName: "Description",
                    type:   "text",
                    dbName: "description",
                    defaultValue: ""
                }
            ]
        },
        payments : {
            type : "payment",
            meta: {
                view:  "view/by_name/payment"
            },
            columns: [
                {
                    uiName: "Customer",
                    type:   "fkeyByName", ftype: "customer",
                    dbName: "customer_id",
                    defaultValue: ""
                },{
                    uiName: "Date",
                    type:   "date",
                    dbName: "date",
                    defaultValue: today()
                },{
                    uiName: "From",
                    type:   "fkeyByNameSelfFiltered", ftype: "account",
                    predicate: "item.is_primary && !item.is_secondary",
                    dbName: "from_account_id",
                    defaultFtype: "customer", defaultFfield:"default_pay_in_account"
                },{
                    uiName: "To",
                    type:   "fkeyByNameSelfFiltered", ftype: "account",
                    predicate: "item.is_secondary && !item.is_primary",
                    dbName: "to_account_id"
                },{
                    uiName: "Amount",
                    type:   "money",
                    dbName: "amount"
                }
            ]
        }
    }
};

exports.all = function() {
    return tables;
};
