function assert(condition, message) {
    if (!condition) {
        throw message || "Assertion failed";
    }
}

function addIfdef(stash) 
{
    switch (typeof(stash))
    {
        case "function":
        case "undefined":
        case "boolean":
        return stash;
        case "number":
        return stash.toString(); //:tricky: mustach work around, id does not like 0.
        case "string":
        if (stash === ""){
            return " "; //:tricky: mustach work around, id does not like empty string.
        }else{
            return stash;
        }
        
        case "object":
        switch (Object.prototype.toString.call(stash))
        {
            case "[object Null]":
            return stash;
            case "[object Object]":
            var Result=new Object();
            for ( var i in stash)
            {
                if (typeof stash[i] != "undefined") {
                    Result["ifdef_"+i] = true; 
                }
                Result[i] = addIfdef(stash[i]);
            }
            return Result;
            case "[object Array]":
            var Result=new Array();
            for ( var i in stash)
            {
                Result[i] =  addIfdef(stash[i]);
            }
            return Result;
            default:
            assert(false,"not object or list:" + Object.prototype.toString.call(stash));
        }
        default:
        assert(false,"unhandled type");
    }
}

function listify(i)
{
    if ( typeof i == "undefined" ) {
        return null;
    }
    else if (typeof i == "boolean") {
        return i?[true]:[false];
    }
    else if (Array.isArray(i)) {
        return i;
    }
    else {
        return [i]
    }
}

function CsvOutputStore() {
    var result ="";
    var self=this;
    self.WriteLine = function(list) {
        var is_first=true;
        list.forEach( function(item) {
            if (!is_first) self.result += ", ";
            is_first = false;
            self.result += '"' +item+ '"';
        });
        self.result += "\n";
    }
    self.WriteLines = function (lines) {
        lines.forEach( function(line) {
            self.WriteLine(line);
        });
    }
    self.Result = function () {
        return self.result;
    }
    return self;
}

exports.assert = assert;
exports.addIfdef = addIfdef;
exports.listify = listify;
exports.CsvOutputStore = CsvOutputStore;
