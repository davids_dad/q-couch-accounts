// a library for validations
// over time we expect to extract more helpers and move them here.
exports.init = function(newDoc, oldDoc, userCtx, secObj) {
    var v = {};
    var l = function(){}
    
    v.forbidden = function(message) {    
        throw({forbidden : message});
    };

    v.unauthorized = function(message) {
        throw({unauthorized : message});
    };

    v.assert = function(predicate, message) {
        if (!predicate) v.forbidden(message);
    }
    
    v.isAdmin = function() {
        return userCtx.roles.indexOf('_admin') != -1
    };

    v.require = function() {
        for (var i=0; i < arguments.length; i++) {
            var fieldName = arguments[i];
            message = "The '"+fieldName+"' field is required.";
            if (typeof newDoc[fieldName] == "undefined") v.forbidden(message);
        };
    };

    v.unchanged = function(fieldName) {
        if (oldDoc && oldDoc[fieldName] != newDoc[fieldName]) 
            v.forbidden("You may not change the '"+fieldName+"' field.");
    };

    v.matches = function(fieldName, regex, message) {
        if ( newDoc.hasOwnProperty(fieldName)  && !newDoc[fieldName].match(regex)) {
            message = message || "Format of '"+fieldName+"' field is invalid.";
            v.forbidden(message);    
        }
    };

    v.dateTimeFormat = function(fieldName) {
        if ( newDoc.hasOwnProperty(fieldName) ){
            message =
                "Sorry, '" +newDoc.filedName+ "' in '" +fieldName
                + "' is not a valid UTC date format.";
            v.matches(fieldName, /\d{4}\-\d{2}\-\d{2}T\d{2}:\d{2}:\d{2}(\.\d*)?Z/, message);
        }
    }

    v.dateFormat = function(fieldName) {
        if ( newDoc.hasOwnProperty(fieldName) ){
            message =
                "Sorry, '" +newDoc.filedName+ "' in '" +fieldName
                + "' is not a valid UTC date format.";
            v.matches(fieldName, /\d{4}\-\d{2}\-\d{2}(T\d{2}:\d{2}:\d{2}(\.\d*)?Z)?/, message);
        }
    }

    l.isAnAuthor = function() {
	    //:tricky: checks author role
	    return userCtx.roles.indexOf("author") != -1 || v.isAdmin();
    };

    v.onlyAuthor = function() {
	    v.assert (l.isAnAuthor(),"You must be an author, to edit.");
	    //:tricky: checks author field
	    if (newDoc.author) {
	        v.assert(
		        newDoc.author == userCtx.name,
		        "You may only edit documents that you own: " + userCtx.name);
	    }
    }

    l.isNumber = function(n) {
	    return !isNaN(parseFloat(n)) && isFinite(n);
    };

    v.number = function(fieldName){
        if (newDoc.hasOwnProperty(fieldName) ){
            v.assert( l.isNumber(newDoc[fieldName]) ,
                      fieldName+ ": must be a number" );
        }
    };

    v.integer = function(fieldName){
        if (newDoc.hasOwnProperty(fieldName) ){
            v.matches (fieldName, "[0-9]+", fieldName + ": must be an integer");
        }
    };

    v.postcode = function(fieldName){
        if (newDoc.hasOwnProperty(fieldName) ){
            //:tricky:apparently this is hard to do, so I will defer.
        }
    };

    v._id = function(id, type, name){
        const typeFromId = id.split("_")[0];
        v.assert( type == typeFromId,
                  "id type missmatch: " +type+ " " +id+ " in " +name );
    };

    v.id2 = function(fieldName, fieldType){
        const fullFieldName = fieldName+ "_id";
        if (newDoc.hasOwnProperty(fullFieldName) ){
            const id = newDoc[fullFieldName];
            v._id(id, fieldType, fullFieldName);
        }
    };
    
    v.id = function(fieldName){
        v.id2(fieldName,fieldName);
    };

    v.money = function(fieldName){
        if (newDoc.hasOwnProperty(fieldName) ){
            v.matches (fieldName, "[0-9]+[.][0-9]{2}", fieldName + ": must be  to 2 decimal places");
        }
    };

    v.bool = function(fieldName){
        if (newDoc.hasOwnProperty(fieldName) ){
           v.matches (fieldName, "(true)|(false)", fieldName + ": must be boolean");
        }
    };

    l.isNewOrChanged = function(fieldName) {
        return !oldDoc || oldDoc[fieldName] != newDoc[fieldName]
    }

    v.validType = function(){
        types=Array.prototype.slice.call(arguments);
        v.require("type");
        const type = newDoc.type;
        v._id(newDoc._id, type, "id");
        if (types.indexOf(type) == -1){
            v.assert(false, "invalid type: " +type+ ": Not in: " + types);
        }
    }
    
    return v;
};
